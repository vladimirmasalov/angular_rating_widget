angular.module('rating-module', [])

    // put here response values;
    .controller('ratingCtrl', function ($scope) {
        $scope.response = [
            {id: 1, currentRating: 0},
            {id: 2, currentRating: 1},
            {id: 3, currentRating: 2},
            {id: 4, currentRating: 3},
            {id: 5, currentRating: 4},
            {id: 6, currentRating: 5}
        ];
    })
    .directive('star', function () {
        'use strict';

        return {
            restrict: 'E',
            template: '<div class="star icon-star" ng-class="{selected: s.selected}"></div>',
            replace: true
        }
    })
    .directive('rating', function () {
        'use strict';

        return {
            restrict: 'E',
            template: '<div class="rating" id="rating-{{id}}"><star ng-repeat="s in stars" ng-click="onClick($index)" ng-mouseover="onOver($index)" ng-mouseleave="onLeave()"></star></div>',
            replace: true,
            scope: {
                total: '=max'
            },

            controller: ['$scope', function ($scope) {
                this.refresh = function () {
                    $scope.stars = [];
                    for (var i = 0; i < $scope.total; i += 1) {
                        $scope.stars.push({selected: i < $scope.range_val});
                    }
                };

                this.send = function (id, rating) {
                    console.log(JSON.stringify({id: id, currentRating: rating}));
                };
            }],

            link: function ($scope, element, attr, controller) {
                $scope.id = parseInt($scope.$parent.r.id, 10);
                $scope.range_val = $scope.range_def = parseInt($scope.$parent.r.currentRating, 10);

                $scope.onClick = function (index) {
                    $scope.range_def = $scope.range_val = index + 1;
                    controller.send($scope.id, $scope.range_val);
                };

                $scope.onOver = function (index) {
                    if ($scope.range_val == index + 1) return;
                    $scope.range_val = index + 1;
                };
                $scope.onLeave = function () {
                    if ($scope.range_val !== $scope.range_def) {
                        $scope.range_val = $scope.range_def;
                    }
                };

                $scope.$watch('range_val', controller.refresh)
            }
        }
    });